package com.pc.config.server.service;

import com.pc.config.server.dto.BankDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class BankService {

  public List<BankDTO> getList() {
    final List<BankDTO> bankList = new ArrayList<>();
    
    for (int i = 0; i < 10; i++) {
      bankList.add(BankDTO.builder()
          .parentBankId(String.valueOf(i))
          .localeDataId((long) i)
          .institutionId(i)
          .institutionName("institutionName " + i)
          .routingNo(UUID.randomUUID().toString())
          .defaultDisplayName("defaultDisplayName " + i)
          .defaultLogoPath("defaultLogoPath " + i)
          .disabled(false)
          .plaidInstitutionId(i)
          .finicityInstitutionId(i)
          .dcBankInstitutionId(i)
          .build());
    }
    return bankList;
  }
}
