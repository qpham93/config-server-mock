package com.pc.config.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankDTO {

  private String parentBankId;
  private Long localeDataId;
  private Integer institutionId;
  private String institutionName;
  private String routingNo;
  private String defaultDisplayName;
  private String defaultLogoPath;
  private Boolean disabled;
  private Integer plaidInstitutionId;
  private Integer finicityInstitutionId;
  private Integer dcBankInstitutionId;
}
