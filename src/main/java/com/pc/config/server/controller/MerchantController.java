package com.pc.config.server.controller;

import com.pc.config.server.dto.MerchantConfigResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("merchant")
public class MerchantController {

  @Value("${merchant.encryptKey}")
  private String encryptKey;


  @GetMapping("encrypt_key")
  public String getEncryptKey() {
    return encryptKey;
  }

  @GetMapping
  public MerchantConfigResponse getMerchantConfig(@RequestParam String merchantId,
                                                  @RequestParam String merchantSubId) {
    boolean representment = true;
    boolean sameDay = false;

    if ("33333333".equals(merchantId)) {
      return null;
    }

    if ("22222222".equals(merchantId)) {
      representment = false;
    }
    if ("2".equals(merchantSubId)) {
      sameDay = true;
    }

    return MerchantConfigResponse.builder()
        .representment(representment)
        .sameDay(sameDay)
        .build();
  }
}
