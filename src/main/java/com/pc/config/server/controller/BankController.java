package com.pc.config.server.controller;

import com.pc.config.server.dto.BankDTO;
import com.pc.config.server.service.BankService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/bank")
@RequiredArgsConstructor
public class BankController {

  private final BankService bankService;

  @GetMapping
  public ResponseEntity<List<BankDTO>> getBankList() {
    return new ResponseEntity<>(bankService.getList(), HttpStatus.OK);
  }
}
